class Employee {
    constructor(firstName, lastName, age, salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.salary = salary;
    }

    get firstName() {
        return this._firstName;
    }

    set firstName(value) {
        this._firstName = value;
    }

    get lastName() {
        return this._lastName;
    }

    set lastName(value) {
        this._lastName = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(firstName, lastName, age, salary, lang) {
        super(firstName, lastName, age, salary);
        this.lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(value) {
        this._lang = value;
    }

    set salary(value) {
        this._salary = value;
    }

    get salary() {
        return this._salary * 3;
    }
}

const jsDev = new Programmer("John", "Smith", "25", 1500, "JavaScript");
const pythonDev = new Programmer("Alexandra", "Norton", "30", 2000, "Python");

console.log(jsDev);
console.log(pythonDev);
